#!/usr/bin/env ruby

require 'optparse'
require 'random_variates'
require 'yaml'

# Evaluates coefficient / x
class Inverse
  attr_reader :factor, :coefficient
  def initialize(factor:, coefficient:)
    fail "Invalid factor id #{factor}" unless factor.is_a?(Integer) && factor > 0
    @factor = factor - 1
    fail "Invalid coefficient #{coefficient}" unless coefficient.is_a?(Numeric)
    @coefficient = coefficient
  end

  def eval_at(x)
    fail "#{x} cannot be 0 for inverse" if x[@factor] == 0
    @coefficient / x[@factor]
  end
end

# Evaluates beta * exp(rate * x)
class Exponential
  attr_reader :factor, :coefficient, :rate
  def initialize(factor:, coefficient:, rate:)
    fail "Invalid factor id #{factor}" unless factor.is_a?(Integer) && factor > 0
    @factor = factor - 1
    fail "Invalid coefficient #{coefficient}" unless coefficient.is_a?(Numeric)
    @coefficient = coefficient
    fail "Invalid rate #{rate}" unless rate.is_a?(Numeric)
    @rate = rate
  end

  def eval_at(x)
    @coefficient * Math.exp(@rate * x[@factor])
  end
end

# Evaluates coefficient / x
class Log
  attr_reader :factor, :coefficient
  def initialize(factor:, coefficient:)
    fail "Invalid factor id #{factor}" unless factor.is_a?(Integer) && factor > 0
    @factor = factor - 1
    fail "Invalid coefficient #{coefficient}" unless coefficient.is_a?(Numeric)
    @coefficient = coefficient
  end

  def eval_at(x)
    fail "#{x} cannot be 0 for log" if x[@factor] == 0
    @coefficient * Math.log(x[@factor])
  end
end

# Evaluates kth order polynomial (integer k)
class Polynomial
  attr_reader :factor, :order, :coeffs
  def initialize(factor:, order:, coeffs:)
    fail "Invalid factor id #{factor}" unless factor.is_a?(Integer) && factor > 0
    @factor = factor - 1
    fail "Invalid order #{order}" unless order.is_a?(Integer) && order > 0
    @order = order
    fail "Must provide 'order' coefficients" unless coeffs.size == @order
    fail "Invalid coefficients #{coeffs}" unless coeffs.all? {|c| c.is_a?(Numeric) }
    @coeffs = coeffs
    @actual_coeffs = coeffs.reverse
  end

  def eval_at(x)
    powers = Array.new(@order) { |k| x[@factor] ** (k+1) }
    powers.zip(@actual_coeffs).map { |x, y| x * y }.sum
  end
end

class Interaction
  attr_reader :factors, :coefficient
  def initialize(factors:, coefficient:)
    fail "Interactions require at least 2 factors" unless factors.size > 1
    fail "Invalid factors #{factors}" unless factors.all? {|f| f.is_a?(Numeric) }
    @factors = factors
    fail "Invalid coefficient #{coefficient}" unless coefficient.is_a?(Numeric)
    @coefficient = coefficient
  end

  def eval_at(x)
    @factors.inject(@coefficient) { |m, i| m * x[i-1] }
  end
end

options = Hash.new(false)
options[:reps] = 1

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: #{$PROGRAM_NAME.gsub(/.*\//,'')} [options] YAMLfilename"
  opts.on('-h', '--help', "Prints this help") do
    puts opts
    exit
  end
  opts.on('-r', '--ranges', "Prints factor ranges, one line per factor") do
    options[:print_ranges] = true
  end
  opts.on('-a', '--all-ranges', "Prints all factor ranges on one line") do
    options[:print_all] = true
  end
  opts.on('-nNUMBER', '--replications=NUMBER', Integer,
    "Replications per design point (default 1)") do |n|
    options[:reps] = n
  end
end
opt_parser.parse!

model_name = ARGV.shift || 'demo_model.yml'

specs = YAML.load( File.open(model_name).read )
ranges = specs[:ranges].map { |o| Object.const_get(o[:class]).new(*o[:endpoints]) }
model = specs[:model].map { |o| Object.const_get(o[:class]).new(**o[:params]) }
noise = Object.const_get(specs[:noise][:class]).new(**specs[:noise][:params])

if options[:print_ranges]
  ranges.each { |r| puts "#{r.begin}\t#{r.end}" }
  exit
elsif options[:print_all]
  puts ranges.inject("") { |m,r| m + " #{r.begin} #{r.end}" }
  exit
end

N = ranges.size

header = Array.new(N) { |i| "x#{i+1}" } << 'y'
puts header.join(',')
while line = gets do
  x = line.strip.split(/[,\s]\s*/).map(&:to_f)
  fail 'Incorrect number of inputs' unless x.length == N
  x.each.with_index do |v, i|
    fail "Input out of bounds: #{x}" unless ranges[i].cover?(v)
  end
  options[:reps].times do
    y = model.inject(specs[:intercept] + noise.next) { |m, o| m + o.eval_at(x) }
    puts (x + [y]).join ','
  end
end
